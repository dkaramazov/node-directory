const express = require('express');
const cors = require('cors');
const error = require('../middleware/error');

const users = require('../routes/users');
const teachers = require('../routes/teachers');
const classes = require('../routes/classes');
const allergy = require('../routes/allergy');
const attendance = require('../routes/attendance');
const child = require('../routes/child');
const interview = require('../routes/interview');
const auth = require('../routes/auth');
module.exports = function (app) {
    app.use(express.json());
    app.use(express.urlencoded({ extended: false}));
    if(process.env.NODE_ENV !== 'production'){
        app.use(cors());
    }
    app.use('/api/users', users);
    app.use('/api/teachers', teachers);
    app.use('/api/classes', classes);
    app.use('/api/allergy', allergy);
    app.use('/api/attendance', attendance);
    app.use('/api/child', child);
    app.use('/api/interview', interview);
    app.use('/api/auth', auth);
    app.use(error);
}