require('winston-mongodb');
require('express-async-errors');
const winston = require('winston');
module.exports = function () {
    winston.exceptions.handle(
        new winston.transports.Console({ colorize: true, prettyPrint: true }),
        new winston.transports.File({ filename: 'unhandledExceptions.log' })
    );

    process.on('unhandledRejection', (ex) => {
        throw ex;
    });

    winston.add(winston.transports.File, { filename: 'errors.log' });
    winston.add(winston.transports.mongoDB, {
        db: process.env.MONGO_URI,
        level: 'error'
    });
}