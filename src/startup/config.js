module.exports = function () {
    if (!process.env.JWT_SECRET) {
        throw new Error('Fatal error: jwt secret not defined');
    }
}