const mongoose = require('mongoose');
const winston = require('winston');
module.exports = function () {
    // map global promise
    mongoose.Promise = global.Promise;
    // connect to db
    mongoose.connect(process.env.MONGO_URI).then(() => {
        winston.info("mongodb connected");
    });
}