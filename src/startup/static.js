const express = require('express');
const path = require('path');
module.exports = function (app) {
    // Serve static assets if in production
    if (process.env.NODE_ENV === 'production') {
        // Set static folder
        app.use(express.static('client'));

        app.get('*', (req, res) => {
            res.sendFile(path.resolve(__dirname, 'client', 'index.html'));
        });
    }
}