const express = require('express');
const router = express.Router();
const { createAttendance, editAttendance, getAttendance, deleteAttendance, getAll } = require('../controllers/attendanceController');
const auth = require('../middleware/auth');

router.post('/', createAttendance);
router.get('/:id', getAttendance);
router.get('/', getAll);
router.put('/:id', editAttendance);
router.delete('/:id', deleteAttendance);

module.exports = router;