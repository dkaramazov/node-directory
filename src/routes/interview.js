const express = require('express');
const router = express.Router();
const { createInterview, editInterview, getInterview, deleteInterview, getAll } = require('../controllers/interviewController');
const auth = require('../middleware/auth');

router.post('/', createInterview);
router.get('/:id', getInterview);
router.get('/', getAll);
router.put('/:id', editInterview);
router.delete('/:id', deleteInterview);

module.exports = router;