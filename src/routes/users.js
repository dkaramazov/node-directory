const express = require('express');
const router = express.Router();
const { getUserInfo, createUser } = require('../controllers/userController');
const auth = require('../middleware/auth');

router.get('/me', auth, getUserInfo);

router.post('/', createUser);

module.exports = router;