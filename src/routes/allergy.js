const express = require('express');
const router = express.Router();
const { createAllergy, editAllergy, getAllergy, deleteAllergy, getAll } = require('../controllers/allergyController');
const auth = require('../middleware/auth');

router.post('/', createAllergy);
router.get('/:id', getAllergy);
router.get('/', getAll);
router.put('/:id', editAllergy);
router.delete('/:id', deleteAllergy);

module.exports = router;