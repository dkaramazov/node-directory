const express = require('express');
const router = express.Router();
const { createClass, editClass, getClass, deleteClass, getAll } = require('../controllers/classController');
const auth = require('../middleware/auth');

router.post('/', createClass);
router.get('/:id', getClass);
router.get('/', getAll);
router.put('/:id', editClass);
router.delete('/:id', deleteClass);

module.exports = router;