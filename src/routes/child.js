const express = require('express');
const router = express.Router();
const { createChild, editChild, getChild, deleteChild, getAll } = require('../controllers/childController');
const auth = require('../middleware/auth');

router.post('/', createChild);
router.get('/:id', getChild);
router.get('/', getAll);
router.put('/:id', editChild);
router.delete('/:id', deleteChild);

module.exports = router;