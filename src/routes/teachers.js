const express = require('express');
const router = express.Router();
const { createTeacher, editTeacher, getTeacher, deleteTeacher, getAll } = require('../controllers/teacherController');
const auth = require('../middleware/auth');

router.post('/', createTeacher);
router.get('/:id', getTeacher);
router.get('/', getAll);
router.put('/:id', editTeacher);
router.delete('/:id', deleteTeacher);

module.exports = router;