const express = require('express');
const cors = require('cors');
const app = express();

require('./startup/logging');
require('./startup/routes')(app);
require('./startup/static')(app);
require('./startup/database')();
require('./startup/config')();
require('./startup/validation')();

module.exports = app;