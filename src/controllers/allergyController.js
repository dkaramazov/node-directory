const _ = require('lodash');
const { Allergy, validate } = require('../models/allergy');

exports.createAllergy = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    allergy = new Allergy(_.pick(req.body, ['description']));
    allergy = await allergy.save();

    res.send(allergy);
}
exports.editAllergy = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const allergy = await Allergy.findByIdAndUpdate(req.params.id,
        {
            description: req.body.description
        }, { new: true });

    if (!allergy) return res.status(404).send('The allergy with the given ID was not found.');

    res.send(allergy);
}
exports.deleteAllergy = async (req, res) => {
    const allergy = await Allergy.findByIdAndRemove(req.params.id);

    if (!allergy) return res.status(404).send('The allergy with the given ID was not found.');

    res.send(allergy);
}
exports.getAllergy = async (req, res) => {
    const allergy = await Allergy.findById(req.params.id);

    if (!allergy) return res.status(404).send('The allergy with the given ID was not found.');

    res.send(allergy);
}
exports.getAll = async (req, res) => {
    const allergy = await Allergy.find().sort('description');
    res.send(allergy);
}