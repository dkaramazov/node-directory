const _ = require('lodash');
const { Attendance, validate } = require('../models/attendance');

exports.createAttendance = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    attendance = new Attendance(_.pick(req.body, ['date', 'students', 'teachers']));
    attendance = await attendance.save();

    res.send(attendance);
}
exports.editAttendance = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const attendance = await Attendance.findByIdAndUpdate(req.params.id,
        {
            students: req.body.students,
            teachers: req.body.teachers
        }, { new: true });

    if (!attendance) return res.status(404).send('The attendance with the given ID was not found.');

    res.send(attendance);
}
exports.deleteAttendance = async (req, res) => {
    const attendance = await Attendance.findByIdAndRemove(req.params.id);

    if (!attendance) return res.status(404).send('The attendance with the given ID was not found.');

    res.send(attendance);
}
exports.getAttendance = async (req, res) => {
    const attendance = await Attendance.findById(req.params.id);

    if (!attendance) return res.status(404).send('The attendance with the given ID was not found.');

    res.send(attendance);
}
exports.getAll = async (req, res) => {
    const attendance = await Attendance.find().sort('description');
    res.send(attendance);
}