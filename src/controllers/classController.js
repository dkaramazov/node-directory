const _ = require('lodash');
const { Class, validate } = require('../models/class');

exports.createClass = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    class_room = new Class(_.pick(req.body, ['name', 'book']));
    class_room = await class_room.save();

    res.send(class_room);
}
exports.editClass = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const class_room = await Class.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            book: req.body.book,
            students: req.body.students,
            teachers: req.body.teachers
        }, { new: true });

    if (!class_room) return res.status(404).send('The class with the given ID was not found.');

    res.send(class_room);
}
exports.deleteClass = async (req, res) => {
    const class_room = await Class.findByIdAndRemove(req.params.id);

    if (!class_room) return res.status(404).send('The class with the given ID was not found.');

    res.send(class_room);
}
exports.getClass = async (req, res) => {
    const class_room = await Class.findById(req.params.id);

    if (!class_room) return res.status(404).send('The class with the given ID was not found.');

    res.send(class_room);
}
exports.getAll = async (req, res) => {
    const class_room = await Class.find().sort('name');
    res.send(class_room);
}