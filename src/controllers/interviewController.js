const _ = require('lodash');
const { Interview, validate } = require('../models/interview');

exports.createInterview = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    interview = new Interview(_.pick(req.body, ['class', 'date', 'level', 'notes', 'student', 'teachers']));
    interview = await interview.save();

    res.send(interview);
}
exports.editInterview = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const interview = await Interview.findByIdAndUpdate(req.params.id,
        {
            description: req.body.description
        }, { new: true });

    if (!interview) return res.status(404).send('The interview with the given ID was not found.');

    res.send(interview);
}
exports.deleteInterview = async (req, res) => {
    const interview = await Interview.findByIdAndRemove(req.params.id);

    if (!interview) return res.status(404).send('The interview with the given ID was not found.');

    res.send(interview);
}
exports.getInterview = async (req, res) => {
    const interview = await Interview.findById(req.params.id)
        .populate('class')
        .populate('student')
        .populate('teachers');

    if (!interview) return res.status(404).send('The interview with the given ID was not found.');

    res.send(interview);
}
exports.getAll = async (req, res) => {
    const interview = await Interview.find().sort('parent.name')
        .populate('class')
        .populate('student')
        .populate('teachers');
    res.send(interview);
}