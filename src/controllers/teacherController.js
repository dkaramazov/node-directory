const _ = require('lodash');
const { Teacher, validate } = require('../models/teacher');

exports.createTeacher = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let teacher = await Teacher.findOne({ email: req.body.email })
    if (teacher) return res.status(400).send('Teacher already registered');

    teacher = new Teacher(_.pick(req.body, ['name', 'email', 'phone']));
    teacher = await teacher.save();

    res.send(teacher);
}
exports.editTeacher = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const teacher = await Teacher.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone
        }, { new: true });

    if (!teacher) return res.status(404).send('The teacher with the given ID was not found.');

    res.send(teacher);
}
exports.deleteTeacher = async (req, res) => {
    const teacher = await Teacher.findByIdAndRemove(req.params.id);

    if (!teacher) return res.status(404).send('The teacher with the given ID was not found.');

    res.send(teacher);
}
exports.getTeacher = async (req, res) => {
    const teacher = await Teacher.findById(req.params.id);

    if (!teacher) return res.status(404).send('The teacher with the given ID was not found.');

    res.send(teacher);
}
exports.getAll = async (req, res) => {
    const teachers = await Teacher.find().sort('name');
    res.send(teachers);
}