const _ = require('lodash');
const { Student, validate } = require('../models/student');

exports.createStudent = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    student = new Student(_.pick(req.body, [
        'name',
        'birthday',
        'phone',
        'email',
        'attendance',
        'class',
        'join_date',
        'languages',
        'created_date',
        'allergy',
        'source',
        'experience',
        'interviews',
        'children',
        'notes'
    ]));
    student = await student.save();

    res.send(student);
}
exports.editStudent = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const student = await Student.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            phone: req.body.phone,
            email: req.body.email,
            birthday: req.body.birthday,
            attendance: req.body.attendance,
            class: req.body.class,
            join_date: req.body.join_date,
            languages: req.body.languages,
            created_date: req.body.created_date,
            allergy: req.body.allergy,
            source: req.body.source,
            interviews: req.body.interviews,
            children: req.body.children,            
            notes: req.body.notes            
        }, { new: true });

    if (!student) return res.status(404).send('The student with the given ID was not found.');

    res.send(student);
}
exports.deleteStudent = async (req, res) => {
    const student = await Student.findByIdAndRemove(req.params.id);

    if (!student) return res.status(404).send('The student with the given ID was not found.');

    res.send(student);
}
exports.getStudent = async (req, res) => {
    const student = await Student.findById(req.params.id)
        .populate('attendance')
        .populate('class')
        .populate('allergy')
        .populate('interviews')
        .populate('children');

    if (!student) return res.status(404).send('The student with the given ID was not found.');

    res.send(student);
}
exports.getAll = async (req, res) => {
    const student = await Student.find().sort('parent.name')
    .populate('attendance')
    .populate('class')
    .populate('allergy')
    .populate('interviews')
    .populate('children');
    
    res.send(student);
}