const _ = require('lodash');
const { Child, validate } = require('../models/child');

exports.createChild = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    child = new Child(_.pick(req.body, ['parent', 'age', 'allergy']));
    child = await child.save();

    res.send(child);
}
exports.editChild = async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const child = await Child.findByIdAndUpdate(req.params.id,
        {
            description: req.body.description
        }, { new: true });

    if (!child) return res.status(404).send('The child with the given ID was not found.');

    res.send(child);
}
exports.deleteChild = async (req, res) => {
    const child = await Child.findByIdAndRemove(req.params.id);

    if (!child) return res.status(404).send('The child with the given ID was not found.');

    res.send(child);
}
exports.getChild = async (req, res) => {
    const child = await Child.findById(req.params.id)
        .populate('parent')
        .populate('allergy');

    if (!child) return res.status(404).send('The child with the given ID was not found.');

    res.send(child);
}
exports.getAll = async (req, res) => {
    const child = await Child.find().sort('parent.name')
        .populate('parent')
        .populate('allergy');
    res.send(child);
}