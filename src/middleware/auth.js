const jwt = require('jsonwebtoken');

module.exports = function auth(req, res, next){
    const token = req.header('x-auth-header');
    if(!token){
        return res.status(401).send('Access is denied. No Token Provided.');
    }

    try{
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.user = decoded;
        next();
    } catch(error){
        return res.send(400).send('Invalid Token.');
    }
}