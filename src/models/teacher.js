const Joi = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Teacher = mongoose.model('Teacher', new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    active: {
        type: Boolean,
        default: true,
        required: true
    },
    class: {
        ref: 'Class',
        type: Schema.Types.ObjectId
    },
    attendance: [{
        ref: 'Attendance',
        type: Schema.Types.ObjectId
    }]
}));

function validateTeacher(teacher) {
    const schema = {
        name: Joi.string().required(),
        email: Joi.string().min(5).max(255).required(),
        phone: Joi.string().min(5).max(50).required(),
        active: Joi.boolean(),
        class: Joi.objectId(),
        attendance: Joi.array(),
    };

    return Joi.validate(teacher, schema);
}

exports.Teacher = Teacher;
exports.validate = validateTeacher;