const Joi = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Interview = mongoose.model('Interview', new mongoose.Schema({
    class: {
        ref: 'Class',
        type: Schema.Types.ObjectId,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    notes: {
        type: String,
        required: true
    },
    student: {
        ref: 'Student',
        type: Schema.Types.ObjectId
    },
    teachers: [{
        ref: 'Teacher',
        type: Schema.Types.ObjectId
    }]
}));

function validateInterview(Interview) {
    const schema = {
        class: Joi.objectId().required(),
        student: Joi.objectId().required(),
        teachers: Joi.objectId().required(),
        level: Joi.string().required(),
        date: Joi.date().required(),
        notes: Joi.string().required()
    };

    return Joi.validate(Interview, schema);
}

exports.Interview = Interview;
exports.validate = validateInterview;