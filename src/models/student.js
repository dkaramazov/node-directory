const Joi = require('joi');
const mongoose = require('mongoose');

const Student = mongoose.model('Student', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    birthday: {
        type: Date
    },
    phone: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    attendance: [{
        ref: 'Attendance',
        type: Schema.Types.ObjectId
    }],
    class: {
        ref: 'Class',
        type: Schema.Types.ObjectId
    },
    join_date: {
        type: Date
    },
    languages: [{
        ref: 'Languages',
        type: Schema.Types.ObjectId
    }],
    created_date: {
        type: Date,
        default: moment
    },
    allergy: {
        ref: 'Allergy',
        type: Schema.Types.ObjectId,
        required: true
    },
    source: {
        ref: 'Source',
        type: Schema.Types.ObjectId
    },
    experience: {
        ref: 'Experience',
        type: Schema.Types.ObjectId
    },
    interviews: [{
        ref: 'Interview',
        type: Schema.Types.ObjectId
    }],
    children: [{
        ref: 'Child',
        type: Schema.Types.ObjectId
    }],
    notes: {
        type: String
    }
}));

function validateStudent(student) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        phone: Joi.string().min(5).max(50).required()        
    };

    return Joi.validate(student, schema);
}

exports.Student = Student;
exports.validate = validateStudent;