const Joi = require('joi');
const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const Attendance = mongoose.model('Attendance', new mongoose.Schema({
    date: {
        type: Date,
        default: moment,
        required: true
    },        
    students: [{
        ref: 'Student',
        type: Schema.Types.ObjectId
    }],   
    teachers: [{
        ref: 'Teacher',
        type: Schema.Types.ObjectId
    }]
}));

function validateAttendance(attendance) {
    const schema = {
        date: Joi.date().min(5).max(50).required(),
        studentsAttendance: Joi.array().required(),
        teachers: Joi.array().required()
    };

    return Joi.validate(attendance, schema);
}

exports.Attendance = Attendance;
exports.validate = validateAttendance;