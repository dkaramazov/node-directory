const Joi = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Class = mongoose.model('Class', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    book: {
        type: String
    },    
    students: [{
        ref: 'Student',
        type: Schema.Types.ObjectId
    }],   
    teachers: [{
        ref: 'Teacher',
        type: Schema.Types.ObjectId
    }]
}));

function validateClass(classroom) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        book: Joi.string().min(5).max(50).required(),
        students: Joi.array(),
        teachers: Joi.array()
    };

    return Joi.validate(classroom, schema);
}

exports.Class = Class;
exports.validate = validateClass;