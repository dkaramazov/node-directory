const Joi = require('joi');
const mongoose = require('mongoose');

const Allergy = mongoose.model('Allergy', new mongoose.Schema({
  description: {
    type: String,
    required: true
  }
}));

function validateAllergy(allergy) {
  const schema = {
    description: Joi.string().required(),
  };

  return Joi.validate(allergy, schema);
}

exports.Allergy = Allergy;
exports.validate = validateAllergy;