const Joi = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Child = mongoose.model('Child', new mongoose.Schema({
  parent: {
    ref: 'Student',
    type: Schema.Types.ObjectId
  },
  age: {
    type: Number,
    required: true
  },
  allergy: {
    ref: 'Allergy',
    type: Schema.Types.ObjectId
  }
}));

function validateChild(child) {
  const schema = {
    parent: Joi.objectId().required(),
    age: Joi.number().required(),
    allergy: Joi.objectId().required()
  };

  return Joi.validate(child, schema);
}

exports.Child = Child;
exports.validate = validateChild;